﻿namespace SISTEMA_DE_FACTURACION
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtNyA = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nudDias = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdNegocio = new System.Windows.Forms.RadioButton();
            this.rdTurista = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbBar = new System.Windows.Forms.CheckBox();
            this.cbTyF = new System.Windows.Forms.CheckBox();
            this.cbCable = new System.Windows.Forms.CheckBox();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnAcerca = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtHospedaje = new System.Windows.Forms.TextBox();
            this.txtExtras = new System.Windows.Forms.TextBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtIVA = new System.Windows.Forms.TextBox();
            this.txtTotalP = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudDias)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre y Apellidos";
            // 
            // txtNyA
            // 
            this.txtNyA.Location = new System.Drawing.Point(151, 17);
            this.txtNyA.Name = "txtNyA";
            this.txtNyA.Size = new System.Drawing.Size(318, 20);
            this.txtNyA.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Dias de ocupación:";
            // 
            // nudDias
            // 
            this.nudDias.Location = new System.Drawing.Point(151, 56);
            this.nudDias.Name = "nudDias";
            this.nudDias.Size = new System.Drawing.Size(55, 20);
            this.nudDias.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdNegocio);
            this.groupBox1.Controls.Add(this.rdTurista);
            this.groupBox1.Location = new System.Drawing.Point(25, 94);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo";
            // 
            // rdNegocio
            // 
            this.rdNegocio.AutoSize = true;
            this.rdNegocio.Location = new System.Drawing.Point(35, 59);
            this.rdNegocio.Name = "rdNegocio";
            this.rdNegocio.Size = new System.Drawing.Size(65, 17);
            this.rdNegocio.TabIndex = 7;
            this.rdNegocio.TabStop = true;
            this.rdNegocio.Text = "Negocio";
            this.rdNegocio.UseVisualStyleBackColor = true;
            this.rdNegocio.CheckedChanged += new System.EventHandler(this.rdNegocio_CheckedChanged);
            // 
            // rdTurista
            // 
            this.rdTurista.AutoSize = true;
            this.rdTurista.Location = new System.Drawing.Point(35, 35);
            this.rdTurista.Name = "rdTurista";
            this.rdTurista.Size = new System.Drawing.Size(57, 17);
            this.rdTurista.TabIndex = 6;
            this.rdTurista.TabStop = true;
            this.rdTurista.Text = "Turista";
            this.rdTurista.UseVisualStyleBackColor = true;
            this.rdTurista.CheckedChanged += new System.EventHandler(this.rdTurista_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbBar);
            this.groupBox2.Controls.Add(this.cbTyF);
            this.groupBox2.Controls.Add(this.cbCable);
            this.groupBox2.Location = new System.Drawing.Point(269, 94);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 100);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Servicios";
            // 
            // cbBar
            // 
            this.cbBar.AutoSize = true;
            this.cbBar.Location = new System.Drawing.Point(23, 68);
            this.cbBar.Name = "cbBar";
            this.cbBar.Size = new System.Drawing.Size(68, 17);
            this.cbBar.TabIndex = 2;
            this.cbBar.Text = "Bar Libre";
            this.cbBar.UseVisualStyleBackColor = true;
            // 
            // cbTyF
            // 
            this.cbTyF.AutoSize = true;
            this.cbTyF.Location = new System.Drawing.Point(23, 45);
            this.cbTyF.Name = "cbTyF";
            this.cbTyF.Size = new System.Drawing.Size(96, 17);
            this.cbTyF.TabIndex = 1;
            this.cbTyF.Text = "Teléfono / Fax";
            this.cbTyF.UseVisualStyleBackColor = true;
            // 
            // cbCable
            // 
            this.cbCable.AutoSize = true;
            this.cbCable.Location = new System.Drawing.Point(23, 22);
            this.cbCable.Name = "cbCable";
            this.cbCable.Size = new System.Drawing.Size(70, 17);
            this.cbCable.TabIndex = 0;
            this.cbCable.Text = "TV Cable";
            this.cbCable.UseVisualStyleBackColor = true;
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(487, 17);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 20);
            this.btnCalcular.TabIndex = 7;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(487, 54);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(75, 23);
            this.btnNuevo.TabIndex = 8;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            // 
            // btnAcerca
            // 
            this.btnAcerca.Location = new System.Drawing.Point(487, 95);
            this.btnAcerca.Name = "btnAcerca";
            this.btnAcerca.Size = new System.Drawing.Size(75, 22);
            this.btnAcerca.TabIndex = 9;
            this.btnAcerca.Text = "Acerca de";
            this.btnAcerca.UseVisualStyleBackColor = true;
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(487, 135);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 10;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Total Hospedaje";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 251);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Total Extras";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(79, 284);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Total";
            // 
            // txtHospedaje
            // 
            this.txtHospedaje.Location = new System.Drawing.Point(116, 215);
            this.txtHospedaje.Name = "txtHospedaje";
            this.txtHospedaje.Size = new System.Drawing.Size(100, 20);
            this.txtHospedaje.TabIndex = 14;
            // 
            // txtExtras
            // 
            this.txtExtras.Location = new System.Drawing.Point(116, 248);
            this.txtExtras.Name = "txtExtras";
            this.txtExtras.Size = new System.Drawing.Size(100, 20);
            this.txtExtras.TabIndex = 15;
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(116, 281);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(100, 20);
            this.txtTotal.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(324, 218);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "IVA";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label7.Location = new System.Drawing.Point(280, 251);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Total a Pagar";
            // 
            // txtIVA
            // 
            this.txtIVA.Location = new System.Drawing.Point(355, 215);
            this.txtIVA.Name = "txtIVA";
            this.txtIVA.Size = new System.Drawing.Size(100, 20);
            this.txtIVA.TabIndex = 19;
            // 
            // txtTotalP
            // 
            this.txtTotalP.Location = new System.Drawing.Point(355, 251);
            this.txtTotalP.Name = "txtTotalP";
            this.txtTotalP.Size = new System.Drawing.Size(100, 20);
            this.txtTotalP.TabIndex = 20;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 337);
            this.Controls.Add(this.txtTotalP);
            this.Controls.Add(this.txtIVA);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.txtExtras);
            this.Controls.Add(this.txtHospedaje);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnAcerca);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.nudDias);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNyA);
            this.Controls.Add(this.label1);
            this.Name = "Form2";
            this.Text = "Sistema de Facturacioon";
            ((System.ComponentModel.ISupportInitialize)(this.nudDias)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNyA;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudDias;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdNegocio;
        private System.Windows.Forms.RadioButton rdTurista;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cbBar;
        private System.Windows.Forms.CheckBox cbTyF;
        private System.Windows.Forms.CheckBox cbCable;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnAcerca;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtHospedaje;
        private System.Windows.Forms.TextBox txtExtras;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtIVA;
        private System.Windows.Forms.TextBox txtTotalP;
    }
}